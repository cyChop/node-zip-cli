import path from 'path';
import {getFileContent, getRelativePath} from '#src/files.js';

describe('getRelativePath(path)', () => {
    it('returns relative path with slashes as separator', () => {
        expect(getRelativePath(path.resolve(process.cwd(), 'src/zip.js')))
            .toEqual('src/zip.js');
    });

    it('works for files that don\'t exist', () => {
        expect(getRelativePath(path.resolve(process.cwd(), 'test/im-not-here.txt')))
            .toEqual('test/im-not-here.txt');
    });
});

describe('getFileContent(path)', () => {
    it('', () => {
        /* Execute */
        const content = getFileContent('index.js');

        /* Control */
        expect(content.toString())
            .toMatch(/import cli from '#src\/cli.js';\r?\n\r?\ncli.parse\(\);\r?\n/);
    });
});
