import path from 'path';
import {addEntryToZip, resolveZipfileName} from '#src/zip.js';

describe('resolveZipfileName(zipfile)', () => {
    it('returns zipfile as is if provided with zip extension', () => {
        /* Prepare */
        const zipfile = 'book.zip';

        /* Execute */
        const result = resolveZipfileName(zipfile);

        /* Control */
        expect(result).toEqual(zipfile);
    });

    it('returns zipfile as is if provided with an extension other than zip', () => {
        /* Prepare */
        const zipfile = 'book.epub';

        /* Execute */
        const result = resolveZipfileName(zipfile);

        /* Control */
        expect(result).toEqual(zipfile);
    });

    it('returns zipfile with zip extension if provided without an extension', () => {
        /* Execute */
        const result = resolveZipfileName('book');

        /* Control */
        expect(result).toEqual('book.zip');
    });

    it('returns the name of current directory with a .zip extension if no name is supplied', () => {
        /* Execute */
        const result = resolveZipfileName(undefined);

        /* Control */
        // Can't hard-code project name as it may have been cloned under a different name
        expect(result).toEqual(`${path.basename(path.resolve(process.cwd()))}.zip`);
    });
});

describe('addEntryToZip(file, zip, recurse)', () => {
    let zip;

    beforeEach(() => {
        zip = jasmine.createSpyObj('zip', ['file', 'folder']);
    });

    it('adds files to zip', () => {
        /* Execute */
        addEntryToZip(path.resolve(process.cwd(), 'package.json'), zip, false);

        /* Control */
        expect(zip.file).toHaveBeenCalledOnceWith('package.json', jasmine.anything());
    });

    it('adds folders to zip non-recursively', () => {
        /* Execute */
        addEntryToZip(path.resolve(process.cwd(), 'spec'), zip, false);

        /* Control */
        expect(zip.file).toHaveBeenCalledTimes(0);
        expect(zip.folder).toHaveBeenCalledOnceWith('spec');
    });

    it('adds folders to zip recursively', () => {
        /* Execute */
        addEntryToZip(path.resolve(process.cwd(), 'spec/support'), zip, true);

        /* Control */
        expect(zip.folder).toHaveBeenCalledOnceWith('spec/support');
        expect(zip.file).toHaveBeenCalledOnceWith('spec/support/jasmine.json', jasmine.anything());
    });

    it("doesn't add inexisting entries", () => {
        /* Execute */
        addEntryToZip(path.resolve(process.cwd(), 'test/im-not-there.txt'), zip, true);

        /* Control */
        expect(zip.folder).toHaveBeenCalledTimes(0);
        expect(zip.file).toHaveBeenCalledTimes(0);
    });
});
