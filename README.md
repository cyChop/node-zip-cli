# zip-cli

A basic, NodeJS implementation of `zip` to run zip commands on all platforms, not depending on it being effectively available on the OS.
The purpose is to be compatible with Linux's `zip` while providing only a restricted subset of the original features.

## Warnings about this project

- This tool is written as a one-time fix for a personal need.
  Although care is taken to provide a context for code quality, it can only evolve into a richer tool with the help of a team and community.
  You can use it in its current state if you wish to, but be aware you do so at your own risk.

- Implementation is based on [JSZip](https://www.npmjs.com/package/jszip).
  As a consequence, it inherits its [limitations](https://stuk.github.io/jszip/documentation/limitations.html).

- Features of `zip` will be implemented based on the need for them.
  If you're looking for one of them and can't find it, please submit an issue and/or a merge request.

## Usage

### Installation

You can install this package with the following spec: `gitlab:cyChop/node-zip-cli#v0.1.0`.
For instance:

```shell
npm install gitlab:cyChop/node-zip-cli#v0.1.0
```

### Execution

You can run this package independently or as an NPM script.

To run as a command, use `npx`:

```shell
npx zip -r packaged.zip folder1 folder2 file
```

To run as a script, in the `scripts` section, declare as follows:

```json
{
  "scripts": {
    "pack": "zip -r packaged.zip folder1 folder2 file"
  }
}
```

### Command

```shell
zip [options] [zipfile] [file...]
```

- zipfile: the name of the zip file (`.zip` extension will be used if no extension is provided)
- files: a list of files or folders to include in the generated zip file
- options:
  - `-r`, `--recurse-paths`: recurse into directories

## Roadmap

### v0.1.0 (alpha)

- Features
  - [x] File inclusion, paths
  - [x] Recursive
- CI/CD
  - [x] Tests
- Documentation
  - [x] README

### v0.2.0 (beta)

- Features
  - [ ] File inclusion, globs
  - [ ] Update, freshen
- CI/CD
  - [ ] Test coverage
  - [ ] Snitch/dependabot (command-line tools, CVEs are a real issue)
  - [ ] Sonarcloud
  - [ ] Qodana?

### v1.0.0?

- CI/CD
  - [ ] Deploy to npm?
- Documentation
  - [ ] JSDoc?
  - [ ] Antora?

## Resources

### Manual

- [explainshell](https://explainshell.com/explain/1/zip)
- [Ubuntu manuals](https://manpages.ubuntu.com/manpages/lunar/en/man1/zip.1.html)

### Help on Debian

```
Copyright (c) 1990-2008 Info-ZIP - Type 'zip "-L"' for software license.
Zip 3.0 (July 5th 2008). Usage:
zip [-options] [-b path] [-t mmddyyyy] [-n suffixes] [zipfile list] [-xi list]
  The default action is to add or replace zipfile entries from list, which
  can include the special name - to compress standard input.
  If zipfile and list are omitted, zip compresses stdin to stdout.
  -f   freshen: only changed files  -u   update: only changed or new files
  -d   delete entries in zipfile    -m   move into zipfile (delete OS files)
  -r   recurse into directories     -j   junk (don't record) directory names
  -0   store only                   -l   convert LF to CR LF (-ll CR LF to LF)
  -1   compress faster              -9   compress better
  -q   quiet operation              -v   verbose operation/print version info
  -c   add one-line comments        -z   add zipfile comment
  -@   read names from stdin        -o   make zipfile as old as latest entry
  -x   exclude the following names  -i   include only the following names
  -F   fix zipfile (-FF try harder) -D   do not add directory entries
  -A   adjust self-extracting exe   -J   junk zipfile prefix (unzipsfx)
  -T   test zipfile integrity       -X   eXclude eXtra file attributes
  -y   store symbolic links as the link instead of the referenced file
  -e   encrypt                      -n   don't compress these suffixes
  -h2  show more help
```
