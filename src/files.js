/*
 * This file provides some file-browsing and file-reading utilities.
 */
// Import Node-related utilities
import path from 'path';
import fs from 'fs';

/**
 * @function getAbsolutePath
 *
 * Computes the absolute path of a file.
 *
 * @param {string} relativePath the path relative to the current working directory
 * @returns {string} the absolute path to the file
 */
export const getAbsolutePath = relativePath => path.resolve(process.cwd(), relativePath);

/**
 * @function getRelativePath
 *
 * Returns the path of the file, relative to the current working directory, using <code>/</code> as path separators.
 *
 * @param {string} filePath the path of the file, either absolute or relative to the current working directory
 * @returns {string} the relative path of the file, from the current working directory
 */
export const getRelativePath = filePath => path.relative(process.cwd(), filePath)
    .replaceAll('\\', '/'); // Make sure to use / as separators, even on Windows

/**
 * @function getFileContent
 *
 * Returns the content of a file without alteration.
 *
 * @param {string} path the file path
 * @returns {Buffer} the file's content
 */
export const getFileContent = path => fs.readFileSync(path);
