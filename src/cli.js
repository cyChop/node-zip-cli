/*
 * This file defines the command-line interface.
 * It imports all logic from dedicated source files.
 */
import {Command} from 'commander';
import zip from './zip.js';

const cli = new Command()
    .command('zip')
    .description('package and compress (archive) files')
    .action(zip)
    .argument('[zipfile]')
    .argument('[file...]')
    .option('-r, --recurse-paths', 'recurse into directories');

export default cli;
