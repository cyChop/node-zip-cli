/*
* This file defines the actual packing algorithm for 'zip'.
*/
// Import code from this project
import {getAbsolutePath, getFileContent, getRelativePath} from '#src/files.js';
// Import Node-related utilities
import path from 'path';
import fs from 'fs';
// Import other useful libs
import JSZip from 'jszip';

// region Target file resolution
/**
 * @function resolveZipfileName
 *
 * Builds the name of the target zipfile if necessary.
 *
 * If a zipfile name is provided, if no file extension is detected, <code>.zip</code> will be added;
 * if the provided zipfile contains an extension, if it is not <code>.zip</code>, it will be preserved.
 *
 * If no zipfile name is provided, the name of the current working directory, suffixed with the <code>.zip</code> extension, will be used.
 *
 * @param {string} zipfile the name of the zip file
 */
export const resolveZipfileName = zipfile => {
    if (zipfile) {
        return /\.[a-zA-Z0-9-]+$/g.test(zipfile) ? zipfile : `${zipfile}.zip`;
    } else {
        return `${path.basename(path.resolve(process.cwd()))}.zip`;
    }
};
// endregion

// region Zipping
/**
 * @function addEntryToZip
 *
 * Add a file or a directory to the zip file.
 *
 * @param {string} file the path to the file to add
 * @param {JSZip} zip the zip file being constructed
 * @param {boolean} recurse <code>true</code> to recurse into directories
 */
export const addEntryToZip = (file, zip, recurse) => {
    try {
        const stat = fs.statSync(file);

        if (stat.isFile()) {

            // We got a file, add it to zip
            const relativeFile = getRelativePath(file);
            console.log(`\tadding: ${relativeFile}`);
            zip.file(relativeFile, getFileContent(file));

        } else if (stat.isDirectory()) {

            // We got a directory, create it
            const relativeFile = getRelativePath(file);
            console.log(`\tadding: ${relativeFile}/`);
            zip.folder(relativeFile);

            // If recurse is active, process each entry under this directory
            if (recurse) {
                fs.readdirSync(file)
                    .forEach(subFile => addEntryToZip(path.resolve(file, subFile), zip, true));
            }
        }
    } catch (err) {
        if (err.code === 'ENOENT') {
            // ENOENT: no such file or directory
            console.warn(`\tzip warning: name not matched: ${getRelativePath(file)}`);
        } else {
            // Not sure what could've happened, throw it and let user decide what should be done
            throw err;
        }
    }
};
// endregion

// region Writing zip file
/**
 * @function finalizeZip
 *
 * Writes the zip file to disc.
 *
 * @param {JSZip} zip the zip file
 * @param {string} targetFilename the target file name
 */
const finalizeZip = (zip, targetFilename) => {
    if (zip.file(/.+/).length === 0) {

        // Nothing was zipped
        console.error(`\nzip error: Nothing to do! (${targetFilename})`);
        process.exit(1); // exit with error to allow detection by automating system

    } else {

        zip
            .generateNodeStream({type: 'nodebuffer', streamFiles: true})
            .pipe(fs.createWriteStream(targetFilename))
            .on('finish', () => {
                // JSZip generates a readable stream with an "end" event,
                // but is piped here in a writable stream which emits a "finish" event.
                console.log(`\nzip success: Zipped ${targetFilename}`);
            });
    }
};
// endregion

// region Putting it all together
export default (zipfile, file, {recursePaths}) => {
    // Immediately resolve target filename in order to fast fail (before actually zipping) if something goes wrong
    const resolvedZipfileName = resolveZipfileName(zipfile);
    // TODO v0.2 check destination is writable: https://nodejs.org/api/fs.html#fs_fs_access_path_mode_callback

    // TODO v0.2 handle update/freshen/replace if file exists
    // Loading existing file: https://stuk.github.io/jszip/documentation/howto/read_zip.html
    const zip = new JSZip();

    // TODO v0.2 handle case of globs (minimatch seems the best candidate)
    file
        .map(getAbsolutePath)
        .forEach(file => addEntryToZip(file, zip, recursePaths));

    finalizeZip(zip, resolvedZipfileName);
}
// endregion
